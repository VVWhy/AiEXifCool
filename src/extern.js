/**
 * @description 这里统一引入外部，全局的扩展
 * @author ian sun
 */

// /-----------------------------------------------------------
// /CSS
import 'keen-ui-css'

// /JS
const obj = {}
export default {
  obj
}
